package main

import (
	"github.com/golang/freetype/truetype"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/examples/resources/fonts"
	"github.com/hajimehoshi/ebiten/text"
	"golang.org/x/image/font"
	"image/color"
	"log"
	"strconv"
	"time"
)

var (
	mplusNormalFont font.Face
)

func init() {
	tt, err := truetype.Parse(fonts.MPlus1pRegular_ttf)
	if err != nil {
		log.Fatal(err)
	}

	const dpi = 72
	mplusNormalFont = truetype.NewFace(tt, &truetype.Options{
		Size:    24,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
}

type Game struct {
	n     int
	lines []string
}

func (g *Game) Update(_ *ebiten.Image) error {
	return nil
}

func (g *Game) Layout(_, _ int) (screenWidth, screenHeight int) {
	return 640, 480
}

func (g *Game) Draw(screen *ebiten.Image) {
	for i, s := range g.lines {
		text.Draw(screen, s, mplusNormalFont, 12, 24*(i+1), color.White)
	}
}

func (g *Game) StartCountUp() {
	go func() {
		for {
			g.n++
			var s string
			if g.n%3 == 0 {
				s += "Fizz"
			}
			if g.n%5 == 0 {
				s += "Buzz"
			}
			if s == "" {
				s = strconv.Itoa(g.n)
			}
			g.lines = append(g.lines, s)
			if len(g.lines) > 20 {
				g.lines = g.lines[1:]
			}
			time.Sleep(100 * time.Millisecond)
		}
	}()
}

func main() {
	g := &Game{}
	g.StartCountUp()
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Fizz Buzz")
	ebiten.SetRunnableInBackground(true)
	if err := ebiten.RunGame(g); err != nil {
		log.Fatalln(err)
	}
}
