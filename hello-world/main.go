package main

import (
	"github.com/golang/freetype/truetype"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/examples/resources/fonts"
	"github.com/hajimehoshi/ebiten/text"
	"golang.org/x/image/font"
	"image/color"
	"log"
)

var (
	mplusNormalFont font.Face
	mplusBigFont    font.Face
)

func init() {
	tt, err := truetype.Parse(fonts.MPlus1pRegular_ttf)
	if err != nil {
		log.Fatal(err)
	}

	const dpi = 72
	mplusNormalFont = truetype.NewFace(tt, &truetype.Options{
		Size:    24,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
	mplusBigFont = truetype.NewFace(tt, &truetype.Options{
		Size:    48,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
}

type Game struct {
	ebiten.Game
}

func (g *Game) Update(_ *ebiten.Image) error {
	return nil
}

func (g *Game) Layout(_, _ int) (int, int) {
	return 640, 480
}

func (g *Game) Draw(screen *ebiten.Image) {
	text.Draw(screen, "Hello, World", mplusNormalFont, 0, 24, color.White) // font-sizeが24pxなのでずらす
	text.Draw(screen, "こんにちわーるど", mplusBigFont, 0, 24+48, color.White)     // font-sizeが48pxなので、上の行 + 今回の文字サイズ分ずらす
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Hello, World!")
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatalln(err)
	}
}
