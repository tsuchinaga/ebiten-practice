# ebiten practice

ゲームライブラリebitenを使って何か作る練習

* [hajimehoshi/ebiten: A dead simple 2D game library in Go](https://github.com/hajimehoshi/ebiten)
* [Ebiten - A dead simple 2D game library in Go](https://ebiten.org/)

## [hello-world](hello-world)

アルファベット、マルチバイト文字の両方でこんにちわーるど  

* マルチバイト文字を使ったテキストのDraw
* text.Drawのx, yはベースラインの位置なので、文字サイズ分ずらす


## [fizz-buzz](fizz-buzz)
よくあるFizzBuzz

* Drawとは非同期でカウントアップ
* 表示内容を作るのはカウントアップ側で、DrawではImageに書き込む
* 一応、ウィンドウが非アクティブ状態でも動き続けるようにしてる `ebiten.SetRunnableInBackground(true)`

[Fizz Buzz - Wikipedia](https://ja.wikipedia.org/wiki/Fizz_Buzz)
